﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clutch : MonoBehaviour {

	public GameObject timer;
	public GameObject engine;

	Engine EngineScript;
	Timers TimersScript;

	private float[] clutchArray = { 0.3f, 0.25f, 0.2f, 0.1f, 0f };
	private float[] clutchArray2 = { 0.3f, 0.25f, 0.2f, 0.1f, 0f };
	private int currentClutchState;
	private int currentClutchState2;
	public float currentClutchEngagement;
	public float currentClutchEngagement2;
	public float clutchBoost;
	public float clutchBoost2;
	private float clutchTimer;
	private float clutchTimer2;
	private float clutchDropTime;
	private float clutchDropTime2;

	// Use this for initialization
	void Start () {

		EngineScript = engine.GetComponent<Engine> ();
		TimersScript = timer.GetComponent<Timers> ();

		currentClutchEngagement = clutchArray [currentClutchState];
		currentClutchEngagement2 = clutchArray2 [currentClutchState2];
		currentClutchState = 0;
		currentClutchState2 = 0;
		clutchBoost = 0f;
		clutchBoost2 = 0f;
		clutchTimer = 0.5f;
		clutchTimer2 = 0.5f;
		clutchDropTime = 0.5f;
		clutchDropTime2 = 0.5f;
		
	}
	
	// Update is called once per frame
	void Update () {

		// Clutch shit
		if ((TimersScript.startCountdown < 10) && (currentClutchState < 4)) clutchTimer -= Time.deltaTime;	// If car starts to drive, clutch is being released
		if ((TimersScript.startCountdown2 < 10) && (currentClutchState2 < 4)) clutchTimer2 -= Time.deltaTime;	// If car starts to drive, clutch is being released

		if ((EngineScript.engineRpm < EngineScript.maxRpm) && (TimersScript.startCountdown < 10) && (currentClutchState < 4) && (clutchTimer < 0)) 
			{
			currentClutchState++;
			clutchTimer += clutchDropTime;
			} // If car is driving, clutch is released in 4 states, -1 state per clutchdrop time

		if ((EngineScript.engineRpm2 < EngineScript.maxRpm2) && (TimersScript.startCountdown2 < 10) && (currentClutchState2 < 4) && (clutchTimer2 < 0)) 
		{
			currentClutchState2++;
			clutchTimer2 += clutchDropTime2;
		} // If car is driving, clutch is released in 4 states, -1 state per clutchdrop time




		currentClutchEngagement = clutchArray [currentClutchState];		// Reads clutch engagement from clutch array
		currentClutchEngagement2 = clutchArray2 [currentClutchState2];		// Reads clutch engagement from clutch array
		int maxTorqueArray = EngineScript.torqueArray.Length -5;		// Takes maxium torque as a base for clutch boost
		int maxTorqueArray2 = EngineScript.torqueArray2.Length -5;		// Takes maxium torque as a base for clutch boost
		clutchBoost = EngineScript.torqueArray[maxTorqueArray] * currentClutchEngagement;	// Creates clutch boost based on torque
		clutchBoost2 = EngineScript.torqueArray2[maxTorqueArray2] * currentClutchEngagement2;	// Creates clutch boost based on torque

	}
}
