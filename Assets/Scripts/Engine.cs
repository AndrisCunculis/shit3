﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : MonoBehaviour {

	public GameObject engine;
	public GameObject carForEngine;
	public GameObject timer;
	public GameObject clutch;

	CarMovement CarMovementScript;	// Makes carmovement script accessible
	Timers TimersScript;
	Clutch ClutchScript;

	public float engineRpm;	// Engine's current RPM
	public float engineRpm2;	// Engine's current RPM
	private int roundedRpm;		// Rounded RPM, for getting current torque from torque array
	private int roundedRpm2;		// Rounded RPM, for getting current torque from torque array
	public int maxRpm;		// Engine's maximum RPM
	public int maxRpm2;		// Engine's maximum RPM
	private int rpmNormalizer;

	public int[] torqueArray = { 40, 50, 60, 70, 80, 95, 115, 135, 0 }; 	// Torque for each 1k engine rpm
	public int[] torqueArray2 = { 40, 50, 60, 70, 80, 95, 115, 135, 0 }; 	// Torque for each 1k engine rpm
	public int currentTorque;	// Engine's current torque
	public int currentTorque2;	// Engine's current torque

	public int[] airResistanceArray = {1, 2, 3, 4, 5, 7, 9, 11, 13, 15, 18, 21, 24, 27, 30, 34, 38, 42, 46, 50  };	// Air resistance array
	public int[] airResistanceArray2 = {1, 2, 3, 4, 5, 7, 9, 11, 13, 15, 18, 21, 24, 27, 30, 34, 38, 42, 46, 50  };	// Air resistance array
	private int currentAirResistance;	// Air resistance used for calculating RPM increase/decrease
	private int currentAirResistance2;	// Air resistance used for calculating RPM increase/decrease
	private float velocityForArray;
	private float velocityForArray2;

	// Use this for initialization
	void Start () {
		CarMovementScript = carForEngine.GetComponent<CarMovement> ();
		TimersScript = timer.GetComponent<Timers> ();
		ClutchScript = clutch.GetComponent<Clutch> ();


		engineRpm = 0f;
		engineRpm2 = 0f;
		roundedRpm = 0;
		roundedRpm2 = 0;
		maxRpm = 7500;
		maxRpm2 = 7500;
		currentTorque = 0;
		currentTorque2 = 0;
		rpmNormalizer = 5;
		currentAirResistance = 1;
		currentAirResistance2 = 1;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		roundedRpm = (Mathf.RoundToInt (engineRpm)) / 1000;		// Rounds engine's rpm for reading currentTorque from torqueArray
		roundedRpm2 = (Mathf.RoundToInt (engineRpm2)) / 1000;		// Rounds engine's rpm for reading currentTorque from torqueArray
		currentTorque = torqueArray[roundedRpm];	// Reads current torque value from torqueArray
		currentTorque2 = torqueArray2[roundedRpm2];	// Reads current torque value from torqueArray

		// Ensure no out of bounds array indexing
		if (roundedRpm < 0) roundedRpm = 0;
		if (roundedRpm2 < 0) roundedRpm2 = 0;
		if (roundedRpm >= torqueArray.Length) roundedRpm = torqueArray.Length - 1;
		if (roundedRpm2 >= torqueArray2.Length) roundedRpm2 = torqueArray2.Length - 1;


		// Getting air resistance value	
		float velocityForArray = CarMovementScript.wheelVelocity * 10; 	// Makes a suitable number for getting air resistance value from array
		float velocityForArray2= CarMovementScript.wheelVelocity2 * 10; 	// Makes a suitable number for getting air resistance value from array
		int roundedVelocityForArray = Mathf.RoundToInt(velocityForArray); 	// Rounds up a number to get value from array
		int roundedVelocityForArray2 = Mathf.RoundToInt(velocityForArray2); 	// Rounds up a number to get value from array
		currentAirResistance = airResistanceArray[roundedVelocityForArray]; 	// Get's air resistance value from array
		currentAirResistance2 = airResistanceArray2[roundedVelocityForArray2]; 	// Get's air resistance value from array


		// Getting engine RPM
		if ((engineRpm < maxRpm) && (TimersScript.startCountdown <= 6f)) engineRpm +=(((currentTorque * rpmNormalizer) / currentAirResistance) + ClutchScript.clutchBoost);	// Engine acceleration
		if ((engineRpm2 < maxRpm2) && (TimersScript.startCountdown2 <= 6f)) engineRpm2 +=(((currentTorque2 * rpmNormalizer) / currentAirResistance2) + ClutchScript.clutchBoost2);	// Engine acceleration
		if ((engineRpm > 1) && (TimersScript.startCountdown > 6f)) engineRpm -= currentAirResistance; // Drops RPM in neutral state
		if ((engineRpm2 > 1) && (TimersScript.startCountdown2 > 6f)) engineRpm2 -= currentAirResistance2; // Drops RPM in neutral state


	}
}
