﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timers : MonoBehaviour {

	CarMovement CarMovementScript;
	DriverSkills DriverSkillsScript;

	public GameObject Timer;
	public GameObject driver;
	public GameObject car;

	public float startCountdown;	// This starts to count down when the game is started
	public float startCountdown2;	// This starts to count down when the game is started
	private float resultsTimer;		// This is a timer used for results
	private float resultsTimer2;		// This is a timer used for results
	private string resultsTimerConvertedToString;	// Timer results converted to a string
	private string resultsTimerConvertedToString2;	// Timer results converted to a string
	public UnityEngine.UI.Text resultsDisplay;		// Used for displaying results time on a gamescreen
	public UnityEngine.UI.Text resultsDisplay2;		// Used for displaying results time on a gamescreen
	private float raceLength;	// Track length, finish line
	private float resultsTimerCountdown;
	private float resultsTimerCountdown2;

	// Use this for initialization
	void Start () {

		DriverSkillsScript = driver.GetComponent <DriverSkills>();
		CarMovementScript = car.GetComponent <CarMovement> ();

		startCountdown = 12f + DriverSkillsScript.driverReactionTime;	// Adds reaction time for start moment
		startCountdown2 = 12f + DriverSkillsScript.driverReactionTime2;	// Adds reaction time for start moment
		resultsTimerCountdown = 6f;
		resultsTimerCountdown2 = 6f;
		resultsTimer = 0f;
		resultsTimer2 = 0f;
		raceLength = 10f;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		startCountdown -= Time.deltaTime;	// Counts down time from StartCountdown
		startCountdown2 -= Time.deltaTime;	// Counts down time from StartCountdown
		resultsTimerCountdown -= Time.deltaTime;
		resultsTimerCountdown2 -= Time.deltaTime;

		// Results timer
		if ((resultsTimerCountdown <= 0f) && ( CarMovementScript.distanceTraveled < raceLength))  resultsTimer += Time.deltaTime;	// Starts adding time at the start and stops when finishline is reached
		if ((resultsTimerCountdown2 <= 0f) && ( CarMovementScript.distanceTraveled2 < raceLength))  resultsTimer2 += Time.deltaTime;	// Starts adding time at the start and stops when finishline is reached
		resultsTimerConvertedToString = resultsTimer.ToString ("F3");	// Converts time to a string to display on gamescreen
		resultsTimerConvertedToString2 = resultsTimer2.ToString ("F3");	// Converts time to a string to display on gamescreen
		resultsDisplay.text = "Time: " + resultsTimerConvertedToString;		// Shows the timer on gamescreen
		resultsDisplay2.text = "Time: " + resultsTimerConvertedToString2;		// Shows the timer on gamescreen

		if (CarMovementScript.distanceTraveled > raceLength) startCountdown += 10;	// Puts in neutral after finish
		if (CarMovementScript.distanceTraveled2 > raceLength) startCountdown2 += 10;	// Puts in neutral after finish

	}
}
