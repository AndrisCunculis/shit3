﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChristmasTree : MonoBehaviour {

	Timers TimersScript;
	DriverSkills DriverSkillsScript;

	public GameObject driver;
	public GameObject timer;

	public GameObject blueLight;
	public GameObject orangeLight1;
	public GameObject orangeLight2;
	public GameObject orangeLight3;
	public GameObject greenLight;
	public GameObject redLight;

	public GameObject blueLight_2;
	public GameObject orangeLight1_2;
	public GameObject orangeLight2_2;
	public GameObject orangeLight3_2;
	public GameObject greenLight_2;
	public GameObject redLight_2;

	private float falseStartCheck;
	private float falseStartCheck2;


	// Use this for initialization
	void Start () {
		DriverSkillsScript = driver.GetComponent <DriverSkills>();
		TimersScript = timer.GetComponent <Timers>();

		blueLight.SetActive (false);
		orangeLight1.SetActive (false);
		orangeLight2.SetActive (false);
		orangeLight3.SetActive (false);
		greenLight.SetActive (false);
		redLight.SetActive (false);

		blueLight_2.SetActive (false);
		orangeLight1_2.SetActive (false);
		orangeLight2_2.SetActive (false);
		orangeLight3_2.SetActive (false);
		greenLight_2.SetActive (false);
		redLight_2.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
		falseStartCheck = (DriverSkillsScript.driverReactionTime);	// If reaction time is under 0, it's false start
		falseStartCheck2 = (DriverSkillsScript.driverReactionTime2);	// If reaction time is under 0, it's false start

		if (TimersScript.startCountdown <= 10f) blueLight.SetActive (true);
		if (TimersScript.startCountdown <= 7.5f) orangeLight1.SetActive (true);
		if (TimersScript.startCountdown <= 7f) orangeLight2.SetActive (true);
		if (TimersScript.startCountdown <= 6.5f) orangeLight3.SetActive (true);
		if (TimersScript.startCountdown <= 6f) greenLight.SetActive (true);
		if ((TimersScript.startCountdown <= 6f) && (falseStartCheck < 0f)) redLight.SetActive(true);	// Checks if driver hasn't made a false start

		if (TimersScript.startCountdown2 <= 10f) blueLight_2.SetActive (true);
		if (TimersScript.startCountdown2 <= 7.5f) orangeLight1_2.SetActive (true);
		if (TimersScript.startCountdown2 <= 7f) orangeLight2_2.SetActive (true);
		if (TimersScript.startCountdown2 <= 6.5f) orangeLight3_2.SetActive (true);
		if (TimersScript.startCountdown2 <= 6f) greenLight_2.SetActive (true);
		if ((TimersScript.startCountdown2 <= 6f) && (falseStartCheck2 < 0f)) redLight_2.SetActive(true);	// Checks if driver hasn't made a false start

		
	}
}
