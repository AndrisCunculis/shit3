﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour {

	public GameObject car;
	public GameObject car2;
	public GameObject frontTire;
	public GameObject frontTire2;
	public GameObject rearTire;
	public GameObject rearTire2;
	public GameObject engine;
	public GameObject clutch;
	public GameObject gearbox;
	public GameObject timer;

	Engine engineScript;
	GearBox gearboxScript;
	Clutch clutchScript;
	Timers timersScript;
	DriverSkills driverSkillsScript;

	public int velocityNormalizer;

	public float wheelVelocity;
	public float wheelVelocity2;
	public float distanceTraveled;
	public float distanceTraveled2;
	public float carVelocity;
	public float carVelocity2;
	public float currentTraction;
	public float currentTraction2;

	private float carLift;
	private float carLift2;
	public SpriteRenderer carBody;
	public SpriteRenderer carBody2;
	public Vector3 carLiftVector;
	public Vector3 carLiftVector2;
	public Vector3 carLiftVectorBack;
	public Vector3 carLiftVectorBack2;
	public float maxCarLift;
	public float maxCarLift2;
	public float carRotation;
	public float carRotation2;


	// Use this for initialization
	void Start () {

		engineScript = engine.GetComponent<Engine> ();
		gearboxScript = gearbox.GetComponent<GearBox> ();
		timersScript = timer.GetComponent<Timers> ();
       
		wheelVelocity = 0f;
		wheelVelocity2 = 0f;
		distanceTraveled = 0f;
		distanceTraveled2 = 0f;
		velocityNormalizer = 20000;
		currentTraction = 0f;
		currentTraction2 = 0f;
		carLift = 0.1f;
		carLift2 = 0.1f;
		maxCarLift = 0.01f;
		maxCarLift2 = 0.01f;
	}


	// Update is called once per frame
	void Update () {
		
	   	distanceTraveled += (wheelVelocity * Time.deltaTime);
		distanceTraveled2 += (wheelVelocity2 * Time.deltaTime);


		// Getting wheel velocity
		wheelVelocity = (engineScript.engineRpm * gearboxScript.gearEndRatio) / velocityNormalizer;
		wheelVelocity2 = (engineScript.engineRpm2 * gearboxScript.gearEndRatio2) / velocityNormalizer;


		// Getting car's velocity
		if ((currentTraction < 1f) && (engineScript.engineRpm > 1)) currentTraction+= Time.deltaTime / 2;
		if ((currentTraction2 < 1f) && (engineScript.engineRpm2 > 1)) currentTraction2+= Time.deltaTime / 2;
		carVelocity = wheelVelocity * currentTraction;
		carVelocity2 = wheelVelocity2 * currentTraction2;



		// Car movement
		car.transform.position += new Vector3(carVelocity, 0, 0);
		car2.transform.position += new Vector3(carVelocity2, 0, 0);
		frontTire.transform.position += new Vector3(carVelocity, 0, 0);
		frontTire2.transform.position += new Vector3(carVelocity2, 0, 0);
		rearTire.transform.position += new Vector3(carVelocity, 0, 0);
		rearTire2.transform.position += new Vector3(carVelocity2, 0, 0);
		frontTire.transform.Rotate (Vector3.back * wheelVelocity * 100);
		frontTire2.transform.Rotate (Vector3.back * wheelVelocity2 * 100);
		rearTire.transform.Rotate (Vector3.back * carVelocity * 100);
		rearTire2.transform.Rotate (Vector3.back * carVelocity2 * 100);


		// Car rotation on acceleration
		carRotation = carBody.transform.rotation.z;
		carRotation2 = carBody2.transform.rotation.z;
		carLiftVector = Vector3.forward * carLift;
		carLiftVector2 = Vector3.forward * carLift2;
		if ((engineScript.engineRpm < engineScript.maxRpm) && (timersScript.startCountdown < 6f) && (carRotation < maxCarLift)) carBody.transform.Rotate (carLiftVector);
		if ((engineScript.engineRpm2 < engineScript.maxRpm2) && (timersScript.startCountdown2 < 6f) && (carRotation2 < maxCarLift2)) carBody2.transform.Rotate (carLiftVector2);

		// Car rotation in neutral
		carRotation = carBody.transform.rotation.z;
		carRotation2 = carBody2.transform.rotation.z;
		carLiftVectorBack = (Vector3.back * carLift) / 2; 	// On deceleration car rotates 2x slower
		carLiftVectorBack2 = (Vector3.back * carLift2) / 2; 	// On deceleration car rotates 2x slower
		if ((timersScript.startCountdown > 6f) && (carRotation > 0)) carBody.transform.Rotate (carLiftVectorBack);
		if ((timersScript.startCountdown2 > 6f) && (carRotation2 > 0)) carBody2.transform.Rotate (carLiftVectorBack2);


			}
}
